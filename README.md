Hillside Racing
===============

Dependencies & Compiling
========================
- Box2D
- SFML

Ubuntu has very old versions of both SFML and Box2D in its repositories. You have to download and compile SFML and Box2D manually to get the newest versions.

We provide a script which pulls in the necessary dependencies for compiling SFML/Box2D on Ubuntu 13.10 (tested on a fresh install in a virtual machine)

Ubuntu
------
Run ./setup-ubuntu.sh
sudo apt-get install build-essential

Arch Linux (and other distros with recent versions of Box2D and SFML)
---------------------------------------------------------------------
sudo pacman -Syu box2d sfml base-devel
make

Compiling
---------
make

Dependencies
------------

Running
-------
./hsracing
