#ifndef PROPS_HH
#define PROPS_HH

#include "globals.hh"
#include <math.h>

void createTextures(); // creates sky, sun texture
void drawTextures(); // draws cloud, sky, sun
void drawMapTextures(); // draws flowers

extern std::vector<b2Vec2> flower_points;

class Prop {
public:
	Prop(std::string fn) {
		if (!texture.loadFromFile("res/"+fn)) std::cout << "No textures for this sprite" << std::endl;
		texture.setSmooth(true);
//		sprite.setTexture(texture);
	}

	virtual ~Prop() {};
	Prop(const Prop &p) = delete;


protected:
	sf::Texture texture;
	sf::Sprite sprite;

};

class Cloud : public Prop {
public:
	Cloud(std::string fn) : Prop(fn) {
		sf::Sprite temp(texture);
		temp.scale(0.5,0.5);
		clouds.push_back(temp);
	}
	~Cloud() {};

	// generate pattern for cloud
	void regenerate();

	const sf::Vector2f & getPosition() const {
		return clouds.front().getPosition();
	}
	void setPosition(float x, float y) {
		clouds.front().setPosition(x,y);
		regenerate();
	}
	void setPosition(const sf::Vector2f &pos) {
		clouds.front().setPosition(pos);
		regenerate();
	}
	void setOrigin(float x, float y) {
		for(auto it=clouds.begin();it != clouds.end();it++) {
			it->setOrigin(x,y);
		}
	}
	void setOrigin(const sf::Vector2f &origin) {
		for(auto it=clouds.begin();it != clouds.end();it++) {
			it->setOrigin(origin);
		}
	}

	void move(float x, float y) {
		for(auto it=clouds.begin();it != clouds.end();it++) {
			it->move(x,y);
		}
	}
	void move(const sf::Vector2f &mv) {
		for(auto it=clouds.begin();it != clouds.end();it++) {
			it->move(mv);
		}
	}

	const sf::Vector2f getSize() const {
		sf::Vector2f temp(texture.getSize().x, texture.getSize().y);
		return (clouds.back().getPosition()  - clouds.front().getPosition() + temp);
	}

	void draw() {
		for (auto it=clouds.begin(); it != clouds.end();it++) {
			window.draw(*it);
		}
	}
	void printPos() {
		for (auto it=clouds.begin(); it != clouds.end();it++) {
			std::cout << "x:" << it->getPosition().x << " y:" << it->getPosition().y << " " ;
		}
	}

private:
	size_t size;
	std::vector<sf::Sprite> clouds;

};

#endif
