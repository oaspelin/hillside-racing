#include "gamemode.hh"
#include "globals.hh"
#include "vehicle.hh"
#include "map.hh"
#include "utils.hh"
#include "highscore.hh"

#include <iostream>
#include <time.h>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

float32 gamemodeClock;
float32 finishTime;
float32 finishDist;
bool endGame;

float32 time_when_fallen;
bool fallen_prev_frame;

void normalMode(){
	sf::Font font;
	font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");

	sf::Text timeElapsed("",font);
	timeElapsed.setCharacterSize(30);
	timeElapsed.setStyle(sf::Text::Bold);
	timeElapsed.setColor(sf::Color::Black);

	timeElapsed.setPosition(view.getCenter().x-view.getSize().x/2, view.getCenter().y-view.getSize().y/2 + 40.0f);

	std::string s = std::to_string(floorf((gamemodeClock)*100)/100);
	s = s.substr(0, s.length() - 4); // cut out trailing zeroes
	timeElapsed.setString("Time elapsed: " + s + "s");

	if(vehicle->getPos().x >= goal.x && !endGame) {
		finishTime = gamemodeClock;
		endGame = true;
	}

	if(endGame) {
		sf::Text victory("", font);
		victory.setCharacterSize(50);
		victory.setStyle(sf::Text::Bold);
		victory.setColor(sf::Color::Black);

		victory.setString("Congratulations!\n Goal reached in\n" + std::to_string(finishTime) + " seconds");

		sf::FloatRect textRect;
		textRect = victory.getLocalBounds();
		victory.setPosition(view.getCenter().x - textRect.width/2.0f, view.getCenter().y + view.getSize().y/4);

		window.draw(victory);

		if(gamemodeClock - finishTime >= 5 && gameState == InGame) { // return to menu after 5 sec
			// highscores here
			gameState=Menu;
			std::string name=enterName();
			addHighscore(name,current_map.substr(0, current_map.size()-4) ,0,finishTime);
		}
	}
	else {
		window.draw(timeElapsed);
	}
}

void runMode(){
	sf::Font font;
	font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");

	sf::Text timeLeft("",font);
	timeLeft.setCharacterSize(30);
	timeLeft.setStyle(sf::Text::Bold);
	timeLeft.setColor(sf::Color::Black);

	sf::Text distance("",font);
	distance.setCharacterSize(30);
	distance.setStyle(sf::Text::Bold);
	distance.setColor(sf::Color::Black);

	timeLeft.setPosition(view.getCenter().x-view.getSize().x/2, view.getCenter().y-view.getSize().y/2 + 40.0f);
	distance.setPosition(view.getCenter().x-view.getSize().x/2, view.getCenter().y-view.getSize().y/2 + 80.0f);
	int i=int(60-gamemodeClock+0.5);//converts to int and round it up
	timeLeft.setString("Time left: "+std::to_string(i) + "s");

	std::string s = std::to_string(std::max(0.0f, vehicle->getPos().x));
	s = s.substr(0, s.length() - 4);
	distance.setString("Distance: " + s + "m");

	if(gamemodeClock >= 60.0f && !endGame){
		finishDist = vehicle->getPos().x;
		finishTime = gamemodeClock;
		endGame = true;
    }

	if(endGame) {
		sf::Text victory("", font);
		victory.setCharacterSize(50);
		victory.setStyle(sf::Text::Bold);
		victory.setColor(sf::Color::Black);

		victory.setString("Game over!\nYou reached:\n" + std::to_string(finishDist) + " meters!");

		sf::FloatRect textRect;
		textRect = victory.getLocalBounds();
		victory.setPosition(view.getCenter().x - textRect.width/2.0f, view.getCenter().y + view.getSize().y/4);

		window.draw(victory);

		if(gamemodeClock - finishTime >= 5 && gameState == InGame) { // return to menu after 5 sec
			// highscores
			gameState=Menu;
			std::string name=enterName();
			addHighscore(name,"run",finishDist*100, 0);
		}
	}
	else {
		window.draw(timeLeft);
		window.draw(distance);
	}
}

void circusMode(){
	sf::Font font;
	font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");

	sf::Text distance("",font);

	distance.setCharacterSize(30);
	distance.setStyle(sf::Text::Bold);
	distance.setColor(sf::Color::Black);
	distance.setPosition(view.getCenter().x-view.getSize().x/2, view.getCenter().y-view.getSize().y/2 + 80.0f);

	std::string s = std::to_string(std::max(0.0f, vehicle->getPos().x));
	s = s.substr(0, s.length() - 4);
	distance.setString("Distance: " + s + "m");

	sf::Text warning("",font);
	sf::FloatRect textRect;
	warning.setCharacterSize(40);
	warning.setStyle(sf::Text::Bold);

	float32 angle = getClampedAngle(vehicle->body->GetAngle());

	if(endGame) {
		sf::Text victory("", font);
		victory.setCharacterSize(50);
		victory.setStyle(sf::Text::Bold);
		victory.setColor(sf::Color::Black);

		victory.setString("Game over!\nYou reached:\n" + std::to_string(finishDist) + " meters!");

		sf::FloatRect textRect;
		textRect = victory.getLocalBounds();
		victory.setPosition(view.getCenter().x - textRect.width/2.0f, view.getCenter().y + view.getSize().y/4);

		window.draw(victory);

		if(gamemodeClock - finishTime >= 5 && gameState == InGame) { // return to menu after 5 sec
			// highscores
			gameState=Menu;
			std::string name=enterName();
			addHighscore(name,"circus",finishDist*100, 0);
		}
	}
	else {
		if((fabs(angle) > 0.33 * M_PI)){
			if (!fallen_prev_frame)
				time_when_fallen = gamemodeClock;

			fallen_prev_frame = true;
			float32 timeToGetUpright = 3.0f - floor(gamemodeClock - time_when_fallen);
			warning.setString("Get upright in " + std::to_string(int(timeToGetUpright)) + "s to continue!");

			// center text
			textRect = warning.getLocalBounds();
			warning.setPosition(view.getCenter().x - textRect.width/2.0f, view.getCenter().y+view.getSize().y/4);

			// fade in text when timer is low
			warning.setColor(sf::Color(200, 50, 50, std::min(255.0f, 255.0f * float(gamemodeClock - time_when_fallen))));
			window.draw(warning);

			// game over
			if(timeToGetUpright <= 0 && !endGame) {
				finishDist = vehicle->getPos().x;
				finishTime = gamemodeClock;
				endGame = true;
			}
		}
		else {
			fallen_prev_frame = false;
		}

		window.draw(distance);
	}
}

std::string enterName(){
	sf::RenderWindow namewindow(sf::VideoMode(400, 100), "Enter your name");
	sf::View nameview(sf::FloatRect(0,0,namewindow.getSize().x,namewindow.getSize().y));
	window.setVerticalSyncEnabled(true);
	sf::Font font;
	font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");

	sf::Text instruction("Enter your name:", font);
	sf::Text name("",font); //the text from userInput
	sf::FloatRect textRect;
	name.setCharacterSize(40);
	name.setColor(sf::Color::Black);
	instruction.setCharacterSize(40);
	instruction.setColor(sf::Color::Black);
	textRect = name.getLocalBounds();

	std::string str; //the return string
	namewindow.setView(nameview);
	namewindow.setPosition(sf::Vector2i(view.getCenter()));
	name.setPosition(nameview.getCenter().x-50, nameview.getCenter().y);
	instruction.setPosition(nameview.getCenter().x-50, nameview.getCenter().y-80);
	sf::Event event;
	while(namewindow.isOpen()){
		namewindow.draw(instruction);
		namewindow.display();
		namewindow.clear(sf::Color::White);
		while( namewindow.pollEvent(event)){
			if(event.type == sf::Event::TextEntered){
				char c = static_cast<char>(event.text.unicode);
				if(c >= 'A' && c <= 'z'){
					str+=c;
				}
			}
			else if (event.type == sf::Event::KeyPressed) {
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
					if (str.length() > 0) str.pop_back();
					else continue;
				else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
					namewindow.close();
					break;
				}
			}
		else if (event.type == sf::Event::Closed)
			namewindow.close();
		}
	name.setString(str);
	namewindow.draw(name);
	}
	window.setView(view);
	//std::cout<<str<<std::endl;
	return str;
}
