#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "props.hh"
#include "map.hh"
#include <random>

sf::Sprite grass_sprite;
sf::Sprite ground_sprite;

//world features
sf::Texture sun_texture;
sf::Sprite sun_sprite;
sf::Texture sky_texture;
sf::Sprite sky_sprite;
sf::Texture treeBG_texture;
sf::Sprite treeBG_sprite;
sf::Texture tree1_texture;
sf::Sprite tree1_sprite;
sf::Texture tree2_texture;
sf::Sprite tree2_sprite;
sf::Texture flower_texture1;
sf::Texture flower_texture2;
sf::Texture goal_texture;
sf::Sprite goal_sprite;
sf::Texture goal_pole_texture;
sf::Sprite goal_pole_sprite;
std::vector<sf::Sprite> flower_sprites;
std::vector<Cloud*> clouds;
Cloud c1("cloud.png");
Cloud c2("cloud.png");
Cloud c3("cloud.png");
Cloud c4("cloud.png");


void Cloud::regenerate() {

	while (clouds.size() > 1) { clouds.pop_back(); }
	// equation for ellipse (x/a)^2 + (y/b)^2 = 1, a = width/2, b = height /2
	// y = sqrt(1 - (y/b)^2) * a
	// a = width / 2, b = height / 2
//	std::cout << std::endl << clouds.size() << ". x: " <<  clouds.front().getPosition().x << " y: " << clouds.front().getPosition().y << std::endl;
	float a = (((size_t) (cint(rng) * 1000)) % 100 + 50);
	float b = (((size_t) (cint(rng) * 1000)) % ((int) a/2) + 40);
//	std::cout << "a=" << a << " b=" << b << std::endl;
	for (float x = -a,y=0; x < a; x += texture.getSize().x*0.22) {
		if (fabs(x/a) > 1) { std::cout << "imaginary y" << std::endl;continue;}
		y = sqrt(1-(x*x/(a*a))) * b;
//		std::cout << "x: "<< x << " first y=" << y;
		for (float y2 = -y; y > y2; y -= texture.getSize().y*0.22) {
			// make new sprite to push to clouds
			sf::Sprite temp(texture);
			temp.scale(0.5*(cint(rng)*0.2+0.9),0.5*(cint(rng)*0.2+0.9));
			clouds.push_back(temp);

//			std::cout << "   x: " << (x+a) <<  " y:" << y << std::endl;
			// and set the position of it
			clouds.back().setPosition(x + a + clouds.front().getPosition().x
//			 + 32 *((cint(rng)*0.5)-1) *0.5
			,
			clouds.front().getPosition().y - y
			 + 20 * ((cint(rng)*0.5)-1) *0.5
			);
//			std::cout << clouds.size() << ". x: " <<  clouds.back().getPosition().x << " y: " << clouds.back().getPosition().y << std::endl;
		}
//		std::cout << std::endl;
	}
//	std::cout  << " size=" << clouds.size() << std::endl;
}

void createTextures(){
	// sky texture
	if (!sky_texture.loadFromFile("res/sky.png"))
	{
		std::cout << "unable to load res/sky.png" << std::endl;
	}

	sky_texture.setSmooth(true);
	sky_sprite.setTexture(sky_texture);

	// background tree textures
	if (!treeBG_texture.loadFromFile("res/treebg.png"))
	{
		std::cout << "unable to load res/treebg.png" << std::endl;
	}

	treeBG_texture.setSmooth(true);
	treeBG_sprite.setTexture(treeBG_texture);

	if (!tree1_texture.loadFromFile("res/tree1.png"))
	{
		std::cout << "unable to load res/tree1.png" << std::endl;
	}

	tree1_texture.setSmooth(true);
	tree1_sprite.setTexture(tree1_texture);

	if (!tree2_texture.loadFromFile("res/tree2.png"))
	{
		std::cout << "unable to load res/tree2.png" << std::endl;
	}

	tree2_texture.setSmooth(true);
	tree2_sprite.setTexture(tree1_texture);

    //texture for sun
    if (!sun_texture.loadFromFile("res/sun.png"))
    {
        std::cout << "And God said: let there not be light" << std::endl;
    }

    sun_texture.setSmooth(true);
    sun_sprite.setTexture(sun_texture);
    sun_sprite.setScale(0.5,0.5);

	// goal texture
	if (!goal_texture.loadFromFile("res/goal.png"))
	{
		std::cout << "unable to load res/goal.png" << std::endl;
	}

	goal_texture.setSmooth(true);
	goal_sprite.setTexture(goal_texture);

    //texture for clouds
	c1.setPosition(0,-250);
	c2.setPosition(500,-550);
	c3.setPosition(-300,-450);
	c4.setPosition(700,-350);

    //flowers
    if(!flower_texture1.loadFromFile("res/flower.png") || !flower_texture2.loadFromFile("res/tulip.png"))
    {
        std::cout << "No flowers :/" << std::endl;
    }

    flower_texture1.setSmooth(true);
    flower_texture2.setSmooth(true);

    //flowers into the sprite vector
    for(size_t i=0; i<flower_points.size(); i++)
    {
		sf::Sprite tmp;
		if(i%3==0)
			tmp.setTexture(flower_texture2);
		else
			tmp.setTexture(flower_texture1);
		tmp.setOrigin(0,-50);
		flower_sprites.push_back(tmp);
    }
}

void drawTextures() {
	sky_sprite.setPosition(view.getCenter().x - view.getSize().x/2, view.getCenter().y-view.getSize().y/2);
	sky_sprite.setScale((double)window.getSize().x/(double)sky_texture.getSize().x, (double)window.getSize().y/(double)sky_texture.getSize().y);

	treeBG_sprite.setPosition(view.getCenter().x - view.getSize().x/2, view.getCenter().y*0.88-view.getSize().y/10);
	treeBG_sprite.setScale((double)window.getSize().x/(double)treeBG_texture.getSize().x, (double)window.getSize().y/(double)treeBG_texture.getSize().y);

	//the sun is on a (almost) fixed position to the vehicle
	sun_sprite.setPosition(view.getCenter().x * 0.99 + view.getSize().x/4,
	view.getCenter().y * 0.9  - view.getSize().y/2.25f);

	//Moves the clouds horizontally at different speeds

	// improved cloud
	c1.move(2,0);
	c2.move(2,0);
	c3.move(2,0);
	c4.move(2,0);

	//if the clouds are left to much behind the vehicle their position will be set ahead of the vehicle
	if(view.getCenter().x - c1.getPosition().x > window.getSize().x / 2 + c1.getSize().x + 150) {
		c1.setPosition(view.getCenter().x + window.getSize().x / 2 + 100 ,
		view.getCenter().y-400*cint(rng));
	}
	if(view.getCenter().x - c2.getPosition().x > window.getSize().x / 2 + c2.getSize().x + 150 ) {
		c2.setPosition(view.getCenter().x + window.getSize().x / 2 + 100 ,
		view.getCenter().y-400*cint(rng));
	}
	if(view.getCenter().x - c3.getPosition().x > window.getSize().x / 2 + c3.getSize().x + 150 ) {
		c3.setPosition(view.getCenter().x + window.getSize().x / 2 + 100 ,
		view.getCenter().y-300*cint(rng));
	}
	if(view.getCenter().x - c4.getPosition().x > window.getSize().x / 2 + c4.getSize().x + 150 ) {
		c4.setPosition(view.getCenter().x + window.getSize().x / 2 + 100 ,
		view.getCenter().y-500*cint(rng));
	}
	//if the clouds are too much ahead the vehicle their position will be set behind the vehicle
	if(c1.getPosition().x - view.getCenter().x > window.getSize().x / 2 + 150 ) {
		c1.setPosition(view.getCenter().x - window.getSize().x / 2 - 100 - c1.getSize().x ,
		view.getCenter().y-500*cint(rng));
	}
	if(c2.getPosition().x - view.getCenter().x > window.getSize().x / 2 + 150 ) {
		c2.setPosition(view.getCenter().x - window.getSize().x / 2 - 100 - c2.getSize().x ,
		view.getCenter().y-400*cint(rng));
	}
	if(c3.getPosition().x - view.getCenter().x > window.getSize().x / 2 + 150 ) {
		c3.setPosition(view.getCenter().x - window.getSize().x / 2 - 100 - c3.getSize().x ,
		view.getCenter().y-300*cint(rng));
	}
	if(c4.getPosition().x - view.getCenter().x > window.getSize().x / 2 + 150 ) {
		c4.setPosition(view.getCenter().x - window.getSize().x / 2 - 100 - c4.getSize().x ,
		view.getCenter().y-600*cint(rng));
	}

	goal_sprite.setPosition(goal.x * PX_IN_M ,(goal.y) * PX_IN_M - 500);

	//draws the other sprites
	window.draw(sky_sprite);
	window.draw(treeBG_sprite);
	for (size_t x = 0; x < window.getSize().x*2; x += 30 + (int)(2.5*x) % 10) {
//		if (cint(rng) > 0.5) {
			tree1_sprite.setPosition(view.getCenter().x - window.getSize().x + x, view.getCenter().y*0.88 - window.getSize().y/10 - 40);
			window.draw(tree1_sprite);
//		}
//		else {
//			tree2_sprite.setPosition(view.getCenter().x - window.getSize().x + x, view.getCenter().y*0.88 - window.getSize().y/10 - 40);
//			window.draw(tree2_sprite);
//		}
	}
	window.draw(sun_sprite);
	c1.draw();
	c2.draw();
	c3.draw();
	c4.draw();

	if(gameMode == normal)
		window.draw(goal_sprite);
}


void drawMapTextures(){

    //draws the flowers
    for (size_t i = 0; i< flower_points.size(); i++)
    {
        flower_sprites[i].setPosition(flower_points.at(i).x * PX_IN_M, flower_points.at(i).y * PX_IN_M-83);
		window.draw(flower_sprites[i]);
    }
}

