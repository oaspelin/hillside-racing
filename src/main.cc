#include <iostream>
#include <time.h>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <Box2D/Box2D.h>
#include <random>
#include "game.hh"
#include "globals.hh"
#include "highscore.hh"
#include "gamemode.hh"

// SFML
sf::RenderWindow window(sf::VideoMode(800, 600), "boxtest");
sf::View view(sf::FloatRect(0,0,window.getSize().x,window.getSize().y));
sf::View menuView(sf::FloatRect(0,0,window.getSize().x,window.getSize().y));
sf::Clock Clock;
sf::Clock FPSClock;

// Box2D
b2Vec2 gravity(0.0f, 9.81f); // 9.81 m/s^2
b2World* world;
Vehicle* vehicle;

float32 timeStep;

std::mt19937_64 rng; // random number device
//rng.seed(8765); // some seed
// the random distribution for the clouds Cloud INTervall
std::uniform_real_distribution<float> cint(0.01,1);

float32 prevFrameTime;
//float32 timeStep = 1.0f / 60.0f;
int32 velocityIterations = 8;
int32 positionIterations = 3;


GameState gameState;
GameMode gameMode;
std::vector<std::shared_ptr<Button>> startMenuEntries;
std::vector<std::shared_ptr<Button>> gameMenuEntries;
std::vector<std::shared_ptr<Button>> highscoreEntries;
sf::Sprite hl;
b2Vec2 goal; //x-location of goal
float fps; // avg fps
std::vector<std::string> mapList;


std::vector<struct Score> highscores;

void init() {
	rng.seed(32);
	prevFrameTime = 0;
	gameState = Menu;
	gameMode = notInGame;
	fps = 0;

    //DEBUG:
    mapList = getMapNames(std::string(".svg"));
    current_map = mapList.back();
	// vsync and framerate limit can't be active at the same time :(
	//window.setVerticalSyncEnabled(false);
	window.setFramerateLimit(60);

}

void handleEvents() {
	// movement, these should not be handled as events
	if (!endGame) {
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
			// Rotate left
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				vehicle->leanLeft();
				//vehicle.angularImpulse(-0.0075f);
			// Rotate right
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				vehicle->leanRight();
				//vehicle.angularImpulse(0.0075f);
		}

		//With shift inserted: GODMODE! MUAHAHAHAHAHHA
		//TODO: Remove this???

		else {
			// Move left
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				vehicle->linearImpulse(b2Vec2(-0.05,0));
			// Move right
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				vehicle->linearImpulse(b2Vec2(0.05,0));
			// Move up
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				vehicle->linearImpulse(b2Vec2(0,-0.05));
			// Move down
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
				vehicle->linearImpulse(b2Vec2(0,0.05));
		}
	}


	sf::Event event;
	while (window.pollEvent(event)) {
		if (event.type == sf::Event::Closed)
			window.close();
		else if (event.type == sf::Event::Resized) {
			view = sf::View(sf::FloatRect(0,0,event.size.width, event.size.height));
			menuView = sf::View(sf::FloatRect(0,0,event.size.width, event.size.height));
		}
		else if (event.type == sf::Event::KeyPressed) {
	        //Return to menu from ingame
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
                gameState = Menu;

			// Pressing space after game has ended should return player to menu
			if (endGame && sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
				gameState = Menu;
				if(gameMode==run){
					std::string name=enterName();
					addHighscore(name,"run",finishDist*100, 0);
				}
				else if(gameMode==circus){
					std::string name=enterName();
					addHighscore(name,"circus",finishDist*100, 0);
				}

				else if(gameMode==normal) {
					std::string name=enterName();
					addHighscore(name,current_map.substr(0, current_map.size()-4) ,0,finishTime);
				}
			}
			// Reset position
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && gameMode != circus) {
				vehicle->reset();
			}
			// Gravity toggle
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::G) && sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) {
				if (world->GetGravity() == gravity) {
					world->SetGravity(b2Vec2(0.0f, 0.0f));
				}
				else {
					world->SetGravity(gravity);
					// Wake up all bodies
					//body->SetAwake(true);
				}
			}
			// reset
			if ((sf::Keyboard::isKeyPressed(sf::Keyboard::R) || sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt)) && !endGame) {
				generateWorldPoints(current_map);
				gamemodeClock = 0;
			}
		}
	}
}



int main() {
	init();

	//generate the world points

	//generateWorldPoints("");
	createMenu();

	// define the menu highlight sprite
	sf::Texture hltexture;
	hltexture.loadFromFile("res/menu/highlight.png");
	hltexture.setRepeated(false);
	hltexture.setSmooth(true);
	hl.setTexture(hltexture);

	// Declare and load a font (the x- and y-position for the box in the game)
	sf::Font font;
	font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");
	// Create a text
	sf::Text text("", font);
	text.setCharacterSize(30);
	text.setStyle(sf::Text::Bold);
	text.setColor(sf::Color::Black);
	sf::Color skyColor(130, 240, 250);

	while (window.isOpen()) {

		if (gameState == InGame) {
			// check for input
			handleEvents();

			// run physics simulation
			timeStep = Clock.getElapsedTime().asSeconds() - prevFrameTime;
			gamemodeClock += timeStep;
			prevFrameTime = Clock.getElapsedTime().asSeconds();

			world->Step(timeStep, velocityIterations, positionIterations);

			// run vehicle logic, try to stay upright
			vehicle->think();

			// FPS counter with  moving average
			if (timeStep != 0)
				fps = 0.95f * fps + 0.05f / timeStep;
			text.setString("FPS: " + std::to_string(int(ceil(fps))));
			//printf("%f,%f\n", position.x,position.y);
			//text.setString(std::to_string(vehicle.getPos().x)+" "+std::to_string(vehicle.getPos().y));

			//Move camera
			//TODO: Remove magic numbers
			view.setCenter(vehicle->getPos().x*PX_IN_M+100, vehicle->getPos().y*PX_IN_M-100);
			text.setPosition(view.getCenter() - view.getSize()*0.5f);
			window.setView(view);

			//Draw stuff
			window.clear(skyColor); //Background
			drawTextures();			//Sun, clouds
			vehicle->draw();		//Draw car
			drawMap();				//Map


            //createTextures();
            //TODO: Fix the flowers????
            drawMapTextures();		//Flowers
			window.draw(text);		//Debugtext

			if(gameMode==run){
				runMode();
			}

			else if(gameMode==circus){
				circusMode();
			}

			else if(gameMode==normal) {
				normalMode();
			}

			window.display();
		} else { // we are in the menu
			if (gameState == Menu) {
				window.setView(menuView);
				menuInput(startMenuEntries);
				drawMenu(startMenuEntries);
				window.display();
			}

			else if(gameState == GameMenu){
				gameMode=notInGame;
				window.setView(menuView);
				menuInput(gameMenuEntries);
				drawMenu(gameMenuEntries);
				window.display();
			}
			else if(gameState==HighscoreMenu){
				window.setView(menuView);
				menuInput(highscoreEntries);
				drawMenu(highscoreEntries);
				window.display();
			}
			else if(gameState==showNormalScore) {
				window.setView(menuView);
				highscoreInput();
				drawHighscore(current_map);
				window.display();
			}
			else if(gameState==showRunScore) {
				window.setView(menuView);
				highscoreInput();
//				current_map = "run";
				drawHighscore("run");
				window.display();
			}
			else if(gameState==showCircusScore) {
				window.setView(menuView);
				highscoreInput();
//				current_map = "circus";
				drawHighscore("circus");
				window.display();
			}


			prevFrameTime = Clock.getElapsedTime().asSeconds();
		}
	}
	//void menuDestruct();
	return EXIT_SUCCESS;
}
