#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "map.hh"
#include "globals.hh"
#include "vehicle.hh"
#include "props.hh"
#include <random>

std::vector<b2Vec2> world_points;
sf::VertexArray grass_verts;
sf::VertexArray ground_verts;
sf::Texture grass_texture;
sf::Texture ground_texture;

//world features
std::vector<b2Vec2> flower_points;
std::string current_map;

void generateWorldPoints(std::string mapname){
	delete world;
	delete vehicle;
	b2Vec2 gravity(0.0f, 9.81f); // 9.81 m/s^2
	world = new b2World(gravity);
	vehicle = new Vehicle();

    //Maps in map folder:
	if (mapname.compare("") != 0)
		mapname = "map/" + mapname;

	flower_points = std::vector<b2Vec2>();

	// initialize random number generator, should be done only once
	//std::random_device rd;
	std::mt19937_64 rng;
	rng.seed(time(NULL));

	// defines the random number generators distribution for floating numbers
	std::uniform_real_distribution<float> dis(-0.5f,0.5f);
	//Spawning area that should be the same always
	world_points = std::vector<b2Vec2>();
	world_points.push_back(b2Vec2(-30.0f,-45.0f));
	world_points.push_back(b2Vec2(-10.0f,-1.5f));
	world_points.push_back(b2Vec2(-8.5f,-0.25f));
	world_points.push_back(b2Vec2(-7.5f,0.0f));
	world_points.push_back(b2Vec2(0,0));

	// read in svg map
	if (mapname.compare("") != 0) {
		std::string coords;
		size_t pos;

		bool absolute = false;
		std::ifstream mapfile;
		mapfile.open(mapname);
		if(mapfile) {
			while (getline (mapfile, coords) ) {
				// search for first curve
				// format is:
				// d="m x1,y1 x2,y2..."
				pos = coords.find("d=\"m ", 0);
				absolute = false;

				if (pos >= coords.length()) { // try absolute coords
					pos = coords.find("d=\"M ", 0);
					absolute = true;
				}
				if (pos < coords.length()) { // found a match
					// hardcoded values to match svg format, skip to beginning of first coord and cut out quotation sign at end
					coords = coords.substr(pos+5, coords.length());
					coords = coords.substr(0, coords.length()-1);
					break;
				}
			}
		} else {
			//std::cout << "Failed to load map, generating instead" << std::endl;
			goto map_load_fail; // goto for error handling is fine, right? ;)
		}

		if (coords.empty()) {
			//std::cout << "Failed to load map, generating instead" << std::endl;
			goto map_load_fail;
		}

		std::string coord;
		std::stringstream ss;
		ss << coords;
		b2Vec2 coord_v;

		while (getline(ss, coord, ' ')) {
			//std::cout << "coord is: " << coord << std::endl;
			if (!coord.compare("l") || !coord.compare("m")) { // svg commands triggering relative mode
				absolute = false;
				continue;
			} else if (!coord.compare("L") || !coord.compare("M")) { // svg commands triggering absolute mode
				absolute = true;
				continue;
			}

			pos = coord.find(",", 0);

			coord_v = b2Vec2(atof(coord.substr(0, pos).c_str()), atof(coord.substr(pos+1, coord.length()).c_str()));
			//std::cout << coord_v.x << ", " << coord_v.y << std::endl;
			if (absolute)
				world_points.push_back(b2Vec2(coord_v.x, coord_v.y));
			else
				world_points.push_back(b2Vec2(world_points.back().x + coord_v.x, world_points.back().y + coord_v.y));
        }

        //Every random point is also a flower point
		for (auto it : world_points) {
            if(dis(rng)>0.2f){
				flower_points.push_back(it);

            }
        }
	}
	else {
        map_load_fail:
		//Randomgenerate this many points
		size_t points = 1000;
		for (size_t i = world_points.size(); i<points; i++) {
			world_points.push_back(b2Vec2(world_points.back().x + fabs(dis(rng) + 1.0f), world_points.back().y + dis(rng)));
			//std::cout << world_points.back().x << ", " << world_points.back().y << std::endl;

			//Every random point is also a flower point
			if(dis(rng)>0.2f)
			  flower_points.push_back(world_points[i]);
		}
	}

	// goal is always the same
	world_points.push_back(b2Vec2(world_points.back().x + 4.5f, world_points.back().y));
	goal=world_points.back();
	world_points.push_back(b2Vec2(world_points.back().x + 3.5f, world_points.back().y));
	world_points.push_back(b2Vec2(world_points.back().x + 1.0f, world_points.back().y - 0.25f));
	world_points.push_back(b2Vec2(world_points.back().x + 1.5f, world_points.back().y - 1.25f));
	world_points.push_back(b2Vec2(world_points.back().x + 30.0f, world_points.back().y - 45.0f));


	/*
	 * Now create SFML & box2d objects of the map
	 */

	//Create a body for box2d
	b2BodyDef temp_body;
	temp_body.type = b2_staticBody;
	b2Body* body = world->CreateBody(&temp_body);

	//Create a shape for Box2D based on the points
	b2Vec2* a = &world_points[0];
	b2ChainShape mapChain;
	mapChain.CreateChain(a,world_points.size());

	//And then a fixture for the shape
	b2FixtureDef mapChainFixture;
	mapChainFixture.shape = &mapChain;
	mapChainFixture.density = 1.0f;
	mapChainFixture.friction = 1.0f;
	mapChainFixture.restitution = 0.1f;

	//Apply fixture to body
	body->CreateFixture(&mapChainFixture);


	//************ Textures ************
	int grass_height = 32; // height of grass
	float32 grass_overflow = 0;
	int ground_height = 2048; // height of dirt texture
	float32 ground_overflow = 0;
	float32 epsilon = 0.5; // less bleed at top/bottom of grass texture, which does not tile on y-axis
	int n;

	// texture for grass
	if (!grass_texture.loadFromFile("res/grass.png"))
	{
		std::cout << "Failed to load res/grass.png!" << std::endl;
	}
	if (!ground_texture.loadFromFile("res/ground.png"))
	{
		std::cout << "Failed to load res/ground.png!" << std::endl;
	}
	grass_verts = sf::VertexArray(sf::Quads, world_points.size()*4);
	ground_verts = sf::VertexArray(sf::Quads, world_points.size()*4);

	// for each vertex on ground (.size() - 1 because we don't want a new quad starting at last vertex)
	for (size_t i = 0; i < world_points.size() - 1; i++){
		// width of quad (in pixels)
		float32 width = (world_points.at(i+1).x - world_points.at(i).x) * PX_IN_M;

		// create quads in clockwise order (SFML wants this)
		sf::Vertex v1 = sf::Vertex();
		sf::Vertex v2 = sf::Vertex();
		sf::Vertex v3 = sf::Vertex();
		sf::Vertex v4 = sf::Vertex();

		// grass vertices
		v1.position = sf::Vector2f(world_points.at(i).x * PX_IN_M, world_points.at(i).y * PX_IN_M-grass_height);
		v1.texCoords = sf::Vector2f(grass_overflow, epsilon);

		v2.position = sf::Vector2f(world_points.at(i+1).x * PX_IN_M, world_points.at(i+1).y * PX_IN_M-grass_height);
		v2.texCoords = sf::Vector2f(grass_overflow + width, epsilon);

		v3.position = sf::Vector2f(world_points.at(i+1).x * PX_IN_M, world_points.at(i+1).y * PX_IN_M);
		v3.texCoords = sf::Vector2f(grass_overflow + width, grass_height - epsilon);

		v4.position = sf::Vector2f(world_points.at(i).x * PX_IN_M, world_points.at(i).y * PX_IN_M);
		v4.texCoords = sf::Vector2f(grass_overflow, grass_height - epsilon);

		grass_verts.append(v1);
		grass_verts.append(v2);
		grass_verts.append(v3);
		grass_verts.append(v4);

		// not actually needed - width between points do not exceed 256 pixels
		// make no assumptions - FruitieX >:)
		n = floor(width-grass_overflow)/grass_texture.getSize().x;

		//std::cout << "n: " << n << " ";
		//std::cout << "width: " << width << " ";
		//std::cout << "grass_overflow before: " <<grass_overflow << " ";

		// how much to offset x-texcoord on next quad?
		grass_overflow = (width + grass_overflow - grass_texture.getSize().x * n);
		n = (grass_overflow / grass_texture.getSize().x); // calculate how many times grass_size goes to grass_overflow and
		grass_overflow -= grass_texture.getSize().x * n; // make it between 0 and grass_size

		// ground vertices
		v1.position = sf::Vector2f(world_points.at(i).x * PX_IN_M, world_points.at(i).y * PX_IN_M); //+ grass_height);
		v1.texCoords = sf::Vector2f(ground_overflow, world_points.at(i).y * PX_IN_M);

		v2.position = sf::Vector2f(world_points.at(i+1).x * PX_IN_M, world_points.at(i+1).y * PX_IN_M); // + grass_height);
		v2.texCoords = sf::Vector2f(ground_overflow + width, world_points.at(i+1).y * PX_IN_M);

		v3.position = sf::Vector2f(world_points.at(i+1).x * PX_IN_M, world_points.at(i+1).y * PX_IN_M + grass_height + ground_height);
		v3.texCoords = sf::Vector2f(ground_overflow + width, ground_height + world_points.at(i+1).y * PX_IN_M);

		v4.position = sf::Vector2f(world_points.at(i).x * PX_IN_M, world_points.at(i).y * PX_IN_M + grass_height + ground_height);
		v4.texCoords = sf::Vector2f(ground_overflow, ground_height + world_points.at(i).y * PX_IN_M);

		ground_verts.append(v1);
		ground_verts.append(v2);
		ground_verts.append(v3);
		ground_verts.append(v4);

		// how much to offset x-texcoord on next quad?
		n = floor(width-ground_overflow)/ground_texture.getSize().x;
		ground_overflow = (width + ground_overflow - ground_texture.getSize().x * n);
		n = (ground_overflow / ground_texture.getSize().x); // calculate how many times ground_size goes to ground_overflow and
		ground_overflow -= ground_texture.getSize().x * n; // make it between 0 and ground_size
	}

	grass_texture.setRepeated(true);
	grass_texture.setSmooth(true);
	ground_texture.setRepeated(true);
	ground_texture.setSmooth(true);

    //And draw flowers and stuff for it
    createTextures();
}


void drawMap(){
    window.draw(grass_verts, &grass_texture);
	window.draw(ground_verts, &ground_texture);
}
