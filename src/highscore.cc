#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

#include "globals.hh"

/**************
 *
 * Highscores saved in a struct. Depending on the gamemode there will be sligthly
 * different values. In the normal gamemode, the mapname will be the name of the map.
 * In Circus and Run mode, where the map should be randomgenerated, the mapname will
 * be set to "circus", or "run".
 *
 * The score will be set to the distance a player has made it, whilst the time is the time
 * it took for the player to get how far he got.
 *
 * In this moment only one of them will be used. But in case there would be a tie it would
 * be nice to have the other one to fall back on. This of course is not really likely due
 * to using floats, and in case someone ends up at the same score, well... Then its a tie.
 *
 * ***********/


void randomscore(){
    //Create random highscores
    for (int i = 0; i!=4; i++) {
        struct Score temp;
        temp.name = "hi long name";
        temp.score = 10 + i;
        temp.time = 100.223f - i;
        highscores.push_back(temp);
    }
}


bool writeHighscore(std::string mapname) {


    //Write highscores to file
    std::ofstream myfile ("map/"+mapname+".txt");
    for (auto it : highscores ){
        myfile << it.name << " "<< it.time << " "<<  it.score << " " << std::endl;
    }
    myfile.close();
    return true;
}

bool readHighscore(std::string mapname) {
    /**********
     *
     * Reads in highscores and sorts them
     *
     * *******/


    //remove previous highscores
    highscores.clear();

    std::ifstream myfile ("map/"+mapname+".txt");
    std::string line;

    if (myfile.is_open()){
        while (getline(myfile,line)) {
            //Parse the line, check what type of map it is, score and name
            size_t pos = 0;
            std::vector<std::string> tempLine;
            struct Score tempStruct;
            std::string tempName;

            //Get a vector that contains all stuff
            while ((pos = line.find(" ")) != std::string::npos) {
                tempLine.push_back(line.substr(0,pos));
                //One space
                line.erase(0, pos+ 1);
            }


            // DEBUG
            /**
            for (auto it : tempLine) {
                std::cout << it;
            }
            **/

            //Create a struct, push it to the vector
            //score
            tempStruct.score = atoi(tempLine.back().c_str());
            tempLine.pop_back();

            //time
            tempStruct.time = stof(tempLine.back());
            tempLine.pop_back();

            //Get name
            std::ostringstream s;
            for (auto &it : tempLine ){
                if(&it != &tempLine[0]) {
                    s << " ";
                }
                s << it;
            }

            tempStruct.name = s.str();

            highscores.push_back(tempStruct);
        }
    }
    myfile.close();

    /*******
    //DEBUG:
    for (auto it : highscores) {
        std::cout << "Struct:"  << std::endl;
        std::cout << " name "<< it.name << std::endl;
        std::cout << " score " << it.score << std::endl;
        std::cout << " time " << it.time << std::endl;
    }
    *********/
    sortHighscore(mapname);
    return true;
}

void sortHighscore(std::string mapname) {
    /*************
     *
     * Takes in the mapname, and sorts the highscores.
     * Sorts the "highscores" vector in place.
     * Requires mapname for knowing what gametype it was.
     *
     ************/

    if (mapname != "circus" && mapname != "run") {
        std::sort(highscores.begin(), highscores.end(), compareTime);
    }
    else {
        std::sort(highscores.begin(), highscores.end(), compareLenght);
    }
    while (highscores.size() > 5) { highscores.pop_back(); } // stores max 5 highscores
}

void addHighscore(std::string name,std::string mapname, int score, float time) {
    /***********
     *
     * Reads the current highscore, then adds a new one, and
     * writes the highscores back to file.
     * TODO: Limit highscores to 10 ?
     *
     **********/
    readHighscore(mapname);

    struct Score tempStruct;
    tempStruct.name = name;
    tempStruct.score = score;
    tempStruct.time = time;

    highscores.push_back(tempStruct);
    sortHighscore(mapname);

    writeHighscore(mapname);
}

/***********
 *
 * Two comparefunctions used for sorting the vector.
 *
 * ********/


bool compareTime(const Score &a, const Score &b) {
    if (a.time < b.time) {
        return true;
    }
    return false;
}

bool compareLenght(const Score &a, const Score &b) {
    if (a.score > b.score) {
        return true;
    }
    else if (a.score == b.score) {
        if (a.score > b.score) {
            return true;
        }
    }
    return false;
}
