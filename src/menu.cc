#include <string>
#include <fstream>
#include <vector>
#include <dirent.h> //Directory listing

#include "menu.hh"
#include "map.hh"
#include "globals.hh"

sf::Texture bg_texture;
sf::Sprite bg_sprite;

void Button::onAction() {
	switch (action) {
		case StartGame: gameState = GameMenu; current_map = mapList.front(); break;
		case ResumeGame:
			if (gameMode != notInGame && !endGame) {
				 gameState = InGame;
			}
			break;
		case Quit: window.close();
		case Normal: gameState= InGame; endGame = false; gameMode=normal; generateWorldPoints(current_map); gamemodeClock = 0.0f;; break;
		case Circus: gameState=InGame; endGame = false; gameMode=circus; current_map = ""; generateWorldPoints(""); gamemodeClock = 0.0f;; break;
		case Run: gameState= InGame; endGame = false; gameMode=run; current_map = ""; generateWorldPoints(""); gamemodeClock = 0.0f;; break;
		case Highscore: gameState= HighscoreMenu; break;
		case NormalScores: gameState = showNormalScore;readHighscore(current_map.substr(0,current_map.length()-4)); break;
		case CircusScores: gameState = showCircusScore; readHighscore("circus"); break;
		case RunScores: gameState = showRunScore;readHighscore("run"); break;
		case Back: gameState=Menu;
		default: break;
	}
}

void highscoreInput() {
	sf::Event event;
        while (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed)
                        window.close();
				else if (event.type == sf::Event::Resized) {
					view = sf::View(sf::FloatRect(0,0,event.size.width, event.size.height));
					menuView = sf::View(sf::FloatRect(0,0,event.size.width, event.size.height));
				}
                else if (event.type == sf::Event::KeyPressed) {

                        /***
                         * What does ESC/Q do?
                         ***/
                        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
                           // if (gameState == showRunScore)
                                gameState = HighscoreMenu;
                            /*****
                             * TODO:
                            else if (gameState == NormalScores)
		                        gameState = HighscoreMenu;
                            else if (gameState == CircusScores)
		                        gameState = HighscoreMenu;
                            else if (gameState == RunScores)
                                gameState = HighscoreMenu;
                            *****/
                        }

                        // go to game with Enter
                    //    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) || sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
                      //          menuEnter(menuEntries);
			            // if up key is pressed
                        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
                                if (gameState == showNormalScore) {
					//std::cout << current_map << std::endl;
                    readHighscore(current_map.substr(0,current_map.length()-4));
					for (auto it = mapList.begin(); it != mapList.end();it++) {
						if (*it == current_map) {
							if (it == mapList.begin()) {
								current_map = mapList.back();
                                break;
							}
							else {
								current_map = *(it - 1);
								break;
							}
						}
					}
                    readHighscore(current_map.substr(0,current_map.length()-4));
				}
			            // if right key is pressed
                        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
                                if (gameState == showNormalScore) {
					//std::cout << current_map << std::endl;
					for (auto it = mapList.begin(); it != mapList.end();it++) {
						if (*it == current_map) {
							if (it == mapList.end()-1) {
								current_map = mapList.front();
								break;
							}
							else {
								current_map = *(it + 1);
								break;
							}
						}
					}
				                readHighscore(current_map.substr(0,current_map.length()-4));
                                }
                }
        }
}

void menuInput(std::vector<std::shared_ptr<Button>> menuEntries) {
	sf::Event event;
        while (window.pollEvent(event)) {
                if (event.type == sf::Event::Closed)
                        window.close();
				else if (event.type == sf::Event::Resized) {
					view = sf::View(sf::FloatRect(0,0,event.size.width, event.size.height));
					menuView = sf::View(sf::FloatRect(0,0,event.size.width, event.size.height));
				}
                else if (event.type == sf::Event::KeyPressed) {

                        /***
                         * What does ESC/Q do?
                         ***/
                        if (event.key.code == sf::Keyboard::Escape || event.key.code == sf::Keyboard::Q) {
                            if (gameState == Menu)
                                window.close();
                            else if (gameState == HighscoreMenu)
                                gameState = Menu;
                            else if (gameState == GameMenu)
                                gameState = Menu;
                            else if (gameState == InGame)
                                 gameState = Menu;
                            /*****
                             * TODO:
                            else if (gameState == NormalScores)
		                        gameState = HighscoreMenu;
                            else if (gameState == CircusScores)
		                        gameState = HighscoreMenu;
                            else if (gameState == RunScores)
                                gameState = HighscoreMenu;
                            *****/
                        }

                        // go to game with Enter
                        if (event.key.code == sf::Keyboard::Return || event.key.code == sf::Keyboard::Space)
                                menuEnter(menuEntries);
			            // if up key is pressed
                        if (event.key.code == sf::Keyboard::Up)
                                menuUp(menuEntries);
			            // if down key is pressed
                        if (event.key.code == sf::Keyboard::Down)
                                menuDown(menuEntries);
                        if (event.key.code == sf::Keyboard::Left)
                                if (gameState == GameMenu) {
					//std::cout << current_map << std::endl;
                    readHighscore(current_map.substr(0,current_map.length()-4));
					for (auto it = mapList.begin(); it != mapList.end();it++) {
						if (*it == current_map) {
							if (it == mapList.begin()) {
								current_map = mapList.back();
                                break;
							}
							else {
								current_map = *(it - 1);
								break;
							}
						}
					}
                    readHighscore(current_map.substr(0,current_map.length()-4));
				}
			            // if right key is pressed
                        if (event.key.code == sf::Keyboard::Right)
                                if (gameState == GameMenu) {
					//std::cout << current_map << std::endl;
					for (auto it = mapList.begin(); it != mapList.end();it++) {
						if (*it == current_map) {
							if (it == mapList.end()-1) {
								current_map = mapList.front();
								break;
							}
							else {
								current_map = *(it + 1);
								break;
							}
						}
					}
				                readHighscore(current_map.substr(0,current_map.length()-4));
                                }
                }
        }
}

void menuUp(std::vector<std::shared_ptr<Button>> menuEntries) {

	for (size_t index=0; index < menuEntries.size(); index++) {
		if (menuEntries[index]->isHighlighted()) {
			menuEntries[index]->toggleHighlight();
			if (index == 0) menuEntries[menuEntries.size()-1]->toggleHighlight();
			else menuEntries[index-1]->toggleHighlight();
			break;
		}
	}
}

void menuDown(std::vector<std::shared_ptr<Button>> menuEntries) {

	for (size_t index=0; index <  menuEntries.size(); index++) {
		if (menuEntries[index]->isHighlighted()) {
			menuEntries[index]->toggleHighlight();
			if (index == (menuEntries.size()-1)) menuEntries[0]->toggleHighlight();
			else menuEntries[index+1]->toggleHighlight();
			break;
		}
	}
}

void menuEnter(std::vector<std::shared_ptr<Button>> menuEntries) {

	for (size_t index=0; index <  menuEntries.size(); index++) {
		if (menuEntries[index]->isHighlighted()) {
			menuEntries[index]->onAction();
			break;
		}
	}
}

void createGameMenu()
{
	std::shared_ptr<Button> normal (new Button("Normal",menuActions::Normal,"Reach goal as fast as you can"));
	std::shared_ptr<Button> circus (new Button("Circus",menuActions::Circus,"Survive as far as possible without falling"));
	std::shared_ptr<Button> run (new Button("Run",menuActions::Run,"Get as far as you can in 60 seconds"));
	std::shared_ptr<Button> back (new Button("Back to menu",menuActions::Back));

	normal->toggleHighlight();

	gameMenuEntries.push_back(std::move(normal));
	gameMenuEntries.push_back(std::move(circus));
	gameMenuEntries.push_back(std::move(run));
	gameMenuEntries.push_back(std::move(back));
	/*
	for(size_t index=0; index <  gameMenuEntries.size(); index++) {
		gameMenuEntries[index]->setPosition(view.getCenter().x ,
		view.getCenter().y + ((gameMenuEntries[index]->getHeight())*(index-gameMenuEntries.size()*0.5f)));
		if (gameMenuEntries[index]->isHighlighted())
			hl.setPosition(gameMenuEntries[index]->getPosition());
	}*/
}


void createMenu() {
/*
	Button startGame("startGame.png",menuActions::StartGame);
	Button options("options.png", menuActions::Options);
	Button quitGame("quit.png",menuActions::Quit);

	startGame.toggleHighlight();

	menuEntries.push_back(&startGame);
	menuEntries.push_back(&options);
	menuEntries.push_back(&quitGame);

*/
	std::shared_ptr<Button> startGame (new Button("New Game",menuActions::StartGame));
	std::shared_ptr<Button> resumeGame (new Button("Resume Game",menuActions::ResumeGame));
	std::shared_ptr<Button> options (new Button("Highscores",menuActions::Highscore));
	std::shared_ptr<Button> quitGame (new Button("Quit",menuActions::Quit));
//	Button *options = new Button("options.png", menuActions::Options);
//	Button *quitGame = new Button("quit.png",menuActions::Quit);

	startGame->toggleHighlight();

	startMenuEntries.push_back(std::move(startGame));
	startMenuEntries.push_back(std::move(resumeGame));
	startMenuEntries.push_back(std::move(options));
	startMenuEntries.push_back(std::move(quitGame));

	/*for (size_t index=0; index <  menuEntries.size(); index++) {
		menuEntries[index]->setPosition(view.getCenter().x,
		view.getCenter().y + ((menuEntries[index]->textRect.height*2)*(index-menuEntries.size()*0.5f)));
		if (menuEntries[index]->isHighlighted()) hl.setPosition(menuEntries[index]->textRect.width/2,menuEntries[index]->textRect.height/2);
	}*/

	createGameMenu(); //also creates the sub-menu
	createHighscoreMenu(); //and highscores

	// background texture
	if (!bg_texture.loadFromFile("res/menu/bg.png"))
	{
		std::cout << "failed to load menu background" << std::endl;
	}

	bg_texture.setSmooth(true);
	bg_sprite.setTexture(bg_texture);
//
}

void drawMenu(std::vector<std::shared_ptr<Button>> menuEntries) {
	startMenuEntries[0]->setPosition(menuView.getCenter().x, menuView.getCenter().y-100.0f);
	startMenuEntries[1]->setPosition(menuView.getCenter().x, menuView.getCenter().y);
	startMenuEntries[2]->setPosition(menuView.getCenter().x, menuView.getCenter().y+100.0f);
	startMenuEntries[3]->setPosition(menuView.getCenter().x, menuView.getCenter().y+200.0f);
	gameMenuEntries[0]->setPosition(menuView.getCenter().x, menuView.getCenter().y-150.0f);
	gameMenuEntries[0]->setDescriptionPosition(menuView.getCenter().x, menuView.getCenter().y-110.0f);

	gameMenuEntries[1]->setPosition(menuView.getCenter().x, menuView.getCenter().y-50.0f);
	gameMenuEntries[1]->setDescriptionPosition(menuView.getCenter().x, menuView.getCenter().y-10);

	gameMenuEntries[2]->setPosition(menuView.getCenter().x, menuView.getCenter().y+50.0f);
	gameMenuEntries[2]->setDescriptionPosition(menuView.getCenter().x, menuView.getCenter().y+90.0f);

	gameMenuEntries[3]->setPosition(menuView.getCenter().x, menuView.getCenter().y+160.0f);

	highscoreEntries[0]->setPosition(menuView.getCenter().x, menuView.getCenter().y-150.0f);
	highscoreEntries[1]->setPosition(menuView.getCenter().x, menuView.getCenter().y-50.0f);
	highscoreEntries[2]->setPosition(menuView.getCenter().x, menuView.getCenter().y+50.0f);
	highscoreEntries[3]->setPosition(menuView.getCenter().x, menuView.getCenter().y+160.0f);


		sf::Font font;
		font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");

		sf::Text text2(current_map, font);
		text2.setCharacterSize(20);
		text2.setStyle(sf::Text::Bold);
		text2.setColor(sf::Color::Black);

		text2.setPosition(menuEntries[0]->getPosition().x - 70, menuEntries[0]->getPosition().y+45);
	for (size_t index=0; index <  menuEntries.size(); index++) {
		if (menuEntries[index]->isHighlighted())
			hl.setPosition(menuEntries[index]->getPosition().x-255.0f, menuEntries[index]->getPosition().y -70.0f);
	}
	//window.clear(sf::Color::Blue);
	bg_sprite.setScale(view.getSize().x / bg_texture.getSize().x, view.getSize().y / bg_texture.getSize().y);
	window.draw(bg_sprite);
//	window.draw(text2);

	window.draw(hl);
	if (gameState == GameMenu) window.draw(text2);
	for (auto iter = menuEntries.begin(); iter != menuEntries.end(); iter++) {
		window.draw((*iter)->getText());
		window.draw((*iter)->getDescription());
	}
}

void drawHighscore(std::string hsName) {
	window.clear(sf::Color::Blue);
	bg_sprite.setScale(view.getSize().x / bg_texture.getSize().x, view.getSize().y / bg_texture.getSize().y);
	window.draw(bg_sprite);
	sf::Font font;
	sf::FloatRect tempRect;

	if (!font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf"))
		gameState = HighscoreMenu;
	std::vector<sf::Text> list;
	while (list.size() < 6) { // first entry is the header
		sf::Text temp("", font);
		temp.setCharacterSize(54);
		temp.setStyle(sf::Text::Bold);
		temp.setColor(sf::Color::Black);
		list.push_back(temp);
	}
//   std::cout << current_map << std::endl;
    //Make the header correct name
    if (hsName == "circus"){ list.front().setString("Circus scores");}
    else if (hsName == "run"){ list.front().setString("Run scores");}
    else {
	if (current_map == "") current_map = mapList.front();
	list.front().setString("Normal " + current_map + " scores");
	}
    //std::cout<<highscores.size()<<std::endl;
    for (size_t i = 0; i < highscores.size() ;i++) {

        //Time for circus and run, else score
        if (hsName == "circus" || hsName == "run") {
//                 std::cout << i << ". " << highscores[i].name << " " << highscores[i].time << std::endl;
		        list[i+1].setString(std::to_string(i+1)+". "+highscores[i].name+" "+std::to_string(highscores[i].score / 100) +"."+ std::to_string(highscores[i].score % 100));
        }
        else {
                //std::cout << i << ". " << highscores[i].name << " " << highscores[i].score << std::endl;
		        list[i+1].setString(std::to_string(i+1)+". "+highscores[i].name+" "+std::to_string(highscores[i].time));
        }

        tempRect=list[i+1].getLocalBounds();
		list[i+1].setOrigin(tempRect.left + tempRect.width/2.0f, tempRect.top  + tempRect.height/2.0f);
		list[i+1].setPosition(menuView.getCenter().x,
		menuView.getCenter().y -120+ (i*80));
	}

	tempRect=list.front().getLocalBounds();
	list.front().setOrigin(tempRect.left + tempRect.width/2.0f, tempRect.top  + tempRect.height/2.0f);
	list.front().setPosition(menuView.getCenter().x, menuView.getCenter().y-200);
	for (auto it = list.begin(); it != list.end();it++)
		window.draw(*it);
}

void createHighscoreMenu()
{
	std::shared_ptr<Button> normal (new Button("Normal",menuActions::NormalScores));
	std::shared_ptr<Button> circus (new Button("Circus",menuActions::CircusScores));
	std::shared_ptr<Button> run (new Button("Run",menuActions::RunScores));
	std::shared_ptr<Button> back (new Button("Back to menu",menuActions::Back));

	normal->toggleHighlight();

	highscoreEntries.push_back(std::move(normal));
	highscoreEntries.push_back(std::move(circus));
	highscoreEntries.push_back(std::move(run));
	highscoreEntries.push_back(std::move(back));

}

std::vector<std::string> getMapNames(std::string ext) {
    /*********
     *
     * Takes in an extension, and searches for it in a folder.
     *
     * ******/

    DIR *dir;
    struct dirent *dirent;
    std::string mapdir = "map";

    std::vector<std::string> tempResult;

    if ((dir = opendir("map")) == NULL) {
        std::cout << "Couldn't open mapdir when trying to read mapfile" << std::endl;
        return tempResult;
    }

    while ((dirent = readdir(dir)) != NULL) {
        std::string fileName = dirent->d_name;
        if (fileName.find(ext, (fileName.length() - ext.length())) != std::string::npos)  {
            tempResult.push_back(fileName);
        }
    }
    closedir(dir);

    //Debug
    /*
    for (auto it : tempResult) {
        std::cout << it << std::endl;
    }
    */

    // sort the temporary list so that the entrys will be in order and nicely in the game :)
    std::sort(tempResult.begin(), tempResult.end(), [] (std::string left, std::string right) -> bool { return ((left.compare(right) > 0) ? false : true);  });
    return tempResult;
}
