#ifndef VEHICLE_HH
#define VEHICLE_HH

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

class Vehicle {
	public:
		Vehicle();
		void draw();
		void angularImpulse(float angle);
		void linearImpulse(b2Vec2 dir);
		void leanLeft();
		void leanRight();
		void reset();
		void think();

		b2Vec2 getPos();
		b2Body* body;
		b2Body* wheel;
		b2RevoluteJoint* joint;
		float32 resetTimerSet;
		sf::Clock Clock;

	private:
		sf::Texture tireTexture;
		sf::Texture bikeTexture;
		sf::Sprite tire_sprite;
		sf::Sprite bike_sprite;
};

#endif
