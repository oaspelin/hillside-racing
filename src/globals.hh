#ifndef GLOBALS_ONCE
#define GLOBALS_ONCE

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <Box2D/Box2D.h>
#include "vehicle.hh"
#include "menu.hh"
#include "highscore.hh"

// definitions
#define PX_IN_M 100
#define RAD_TO_DEG 180/M_PI
#define DEG_TO_RAD M_PI/180

// SFML
extern sf::RenderWindow window;
extern sf::View view;
extern sf::View menuView;

// Box2D
extern b2Vec2 gravity;
extern b2World* world;
extern std::vector<b2Vec2> world_points;
extern b2Vec2 goal;
extern float32 timeStep;

// Define random number generator
extern std::mt19937_64 rng;
extern std::uniform_real_distribution<float> cint;

// suggested values by Box2D tutorial
extern int32 velocityIterations;
extern int32 positionIterations;

extern Vehicle* vehicle;
extern float32 prevFrameTime;

// keeping track of gamemode specific stuff
enum GameState { Menu, InGame, GameMenu, HighscoreMenu, showNormalScore, showCircusScore, showRunScore };
extern GameState gameState;

enum GameMode {normal, circus, run, notInGame};
extern GameMode gameMode;

extern float32 gamemodeClock;
extern bool endGame;

extern std::string current_map;
extern std::vector<std::string> mapList;
extern float32 finishDist;
extern float32 finishTime;

// menu definitions
extern std::vector<std::shared_ptr<Button>> startMenuEntries;
extern sf::Sprite hl; // the menu entry highlight sprite
extern std::vector<std::shared_ptr<Button>> gameMenuEntries;
extern std::vector<std::shared_ptr<Button>> highscoreEntries;

extern std::vector<struct Score> highscores;

#endif
