#include "globals.hh"
float32 getClampedAngle(float32 angle) {
	// box2d angles => [ -3.14; 3.14 ]
	angle = fmod(angle, 2 * M_PI);
	if (angle < 0)
		angle += 2 * M_PI;

	// negative angles when leaning left of center
	if (angle > M_PI) {
		angle = 2 * M_PI - angle;
		angle *= -1;
	}

	return angle;
}
