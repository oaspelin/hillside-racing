#include "vehicle.hh"
#include "globals.hh"
#include "utils.hh"
#include <iostream>
#include <algorithm>

//float32 prevFrameTime;

Vehicle::Vehicle() {

	// wheel
	b2BodyDef wheelDef;
	wheelDef.type = b2_dynamicBody;
	wheelDef.position.Set(-2.0f, -1.5f); // origin
	wheelDef.allowSleep = false;
	wheel = world->CreateBody(&wheelDef);

	b2CircleShape circle;
	circle.m_p.Set(0.0f, 0.0f);
	circle.m_radius = 0.25f; // 50 cm diameter on wheel

	b2FixtureDef wheelFixtureDef;
	wheelFixtureDef.shape = &circle;
	wheelFixtureDef.density = 1.0f;
	wheelFixtureDef.friction = 0.9f;
	wheelFixtureDef.restitution = 0.1f;

	wheel->CreateFixture(&wheelFixtureDef);

	// body
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(-2.0f, -2.0f); // above origin
	bodyDef.allowSleep = false;
	body = world->CreateBody(&bodyDef);

	b2PolygonShape box;
	box.SetAsBox(0.025f, 0.5f);

	b2FixtureDef boxFixtureDef;
	boxFixtureDef.shape = &box;
	boxFixtureDef.density = 0.05f;
	boxFixtureDef.friction = 0.9f;

	body->CreateFixture(&boxFixtureDef);

	// joint

	b2RevoluteJointDef jointDef;
	jointDef.Initialize(wheel, body, wheel->GetWorldCenter());
	joint = (b2RevoluteJoint*)world->CreateJoint(&jointDef);
	(void)joint; // g++ be quiet

	if(!tireTexture.loadFromFile("res/tire.png"))
		window.close();
	if(!bikeTexture.loadFromFile("res/bike.png"))
		window.close();

	tireTexture.setSmooth(true);
	bikeTexture.setSmooth(true);

	tire_sprite.setTexture(tireTexture);
	bike_sprite.setTexture(bikeTexture);

	bike_sprite.setOrigin(bikeTexture.getSize().x / 2, bikeTexture.getSize().y / 2);
	bike_sprite.setScale(0.5, 0.5);

	tire_sprite.setOrigin(tireTexture.getSize().x / 2, tireTexture.getSize().y / 2);
	tire_sprite.setScale(0.5, 0.5);
	//boxSprite.setOrigin(boxTexture.getSize().x / 2, boxTexture.getSize().y / 2);

	resetTimerSet = -9999;
	sf::Clock Clock;
}

void Vehicle::draw() {
	b2Vec2 wPosition = wheel->GetWorldCenter();
	float32 wAngle = wheel->GetAngle();
	b2Vec2 bPosition = body->GetWorldCenter();
	float32 bAngle = body->GetAngle();
	tire_sprite.setPosition(PX_IN_M * wPosition.x, PX_IN_M * wPosition.y);
	tire_sprite.setRotation(wAngle * RAD_TO_DEG);
	bike_sprite.setPosition(PX_IN_M * bPosition.x, PX_IN_M * bPosition.y);
	bike_sprite.setRotation(bAngle * RAD_TO_DEG);
	window.draw(bike_sprite);
	window.draw(tire_sprite);
}

b2Vec2 Vehicle::getPos() {
	return body->GetWorldCenter();
}



void Vehicle::angularImpulse(float angle) {
	wheel->ApplyAngularImpulse(angle,0);
}
void Vehicle::linearImpulse(b2Vec2 dir) {
	wheel->ApplyLinearImpulse(dir, wheel->GetWorldCenter(), true);
}

void Vehicle::leanLeft() {
	body->ApplyAngularImpulse(-0.00015, 0);
}

void Vehicle::leanRight() {
	body->ApplyAngularImpulse(0.00015, 0);
}

void Vehicle::think() {
	float32 angle = getClampedAngle(body->GetAngle());

	float32 angularVel = wheel->GetAngularVelocity();

	double SPEED_LIMIT = 50;
	angularVel = std::min(SPEED_LIMIT, std::max(-SPEED_LIMIT, angularVel + angle * 3.0));
	//std::cout << "angle: " << angle << std::endl;
	//std::cout << "angvel: " << angularVel << std::endl;

	// reset function
	if(Clock.getElapsedTime().asSeconds() - resetTimerSet < 1) {
		float32 angle = getClampedAngle(body->GetAngle());
		body->SetAngularVelocity(-angle*3);

		// breaks
		wheel->SetAngularVelocity(wheel->GetAngularVelocity() * 0.5);
	} // else drive, unless we tripped over
	else if(fabs(angle) < 0.33 * M_PI && !endGame) {
		wheel->SetAngularVelocity(angularVel);
	}
	else { // else apply breaks
		wheel->SetAngularVelocity(wheel->GetAngularVelocity() * 0.5);
	}

	// dampen angular velocity
	body->SetAngularVelocity(body->GetAngularVelocity() * 0.85);
}

void Vehicle::reset() {
	resetTimerSet = Clock.getElapsedTime().asSeconds();
}
