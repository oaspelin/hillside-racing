#ifndef HIGHSCORE_ONCE
#define HIGHSCORE_ONCE

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "globals.hh"

struct Score {
    std::string name;
    float time;
    int score;
};

bool writeHighscore(std::string mapname);
bool readHighscore(std::string mapname);
void sortHighscore(std::string gameType);
void addHighscore(std::string name, std::string mapname, int score, float time);

bool compareTime(const Score &a, const Score &b);
bool compareLenght(const Score &a, const Score &b);

#endif
