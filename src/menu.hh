#ifndef MENU_HH
#define MENU_HH
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <Box2D/Box2D.h>
#include <string>
#include <iostream>
#include <vector>

enum menuActions {
	ResumeGame,
	StartGame,
	Highscore,
	Quit,
	Normal,
	Circus,
	Run,
	Back,
	NormalScores,
	CircusScores,
	RunScores,
};


class Button {
public:
	Button(std::string name, menuActions act, std::string specification=""){

		//menubuttons
		font.loadFromFile("./res/fonts/DejaVuSans-Bold.ttf");
		text.setString(name);
		text.setFont(font);
		text.setCharacterSize(70);
		text.setColor(sf::Color::Black);
		text.setStyle(sf::Text::Bold);

		if(name=="Back to menu")
			text.setCharacterSize(40);

		textRect=text.getLocalBounds();
		text.setOrigin(textRect.left + textRect.width/2.0f, textRect.top  + textRect.height/2.0f);

		//descriptions
		description.setString(specification);
		description.setFont(font);
		description.setCharacterSize(20);
		description.setColor(sf::Color::Black);
		textRect=description.getLocalBounds();
		description.setOrigin(textRect.left + textRect.width/2.0f, textRect.top  + textRect.height/2.0f);

		action = act;
	}

	Button(const Button& b) = delete;
	~Button() { }

	void onAction();

	void toggleHighlight() { highlighted = !highlighted; }
	void setPosition(const sf::Vector2f &position) { text.setPosition(position); }
	void setPosition(float x, float y) { text.setPosition(x,y); }

	sf::Sprite getSprite() const { return sprite; }
	sf::Vector2f getPosition() { return text.getPosition(); }
	sf::Vector2f getSize() const { return text.getScale(); }
	float getWidth() const { return text.getScale().x; }
	float getHeight() const { return text.getScale().y; }
	sf::Text getText() const {return text;}

	bool isHighlighted() { return highlighted; }

	void addDescription(std::string s);
	sf::Text getDescription() {return description;}
	void setDescription(sf::Text d) {description=d;}
	void setDescriptionPosition(float x, float y) {description.setPosition(x,y);}

private:
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Font font;
	sf::Text text;
	sf::Text description;
	sf::FloatRect textRect;

	bool highlighted = false; // true if this menu entry is being highlighted
	menuActions action;


};

void menuUp(std::vector<std::shared_ptr<Button>> menuEntries);
void menuDown(std::vector<std::shared_ptr<Button>> menuEntries);
void menuEnter(std::vector<std::shared_ptr<Button>> menuEntries);

void menuInput(std::vector<std::shared_ptr<Button>> menuEntries);
void highscoreInput();
void createMenu();

void drawMenu(std::vector<std::shared_ptr<Button>> menuEntries);
void drawHighscore(std::string hsName);

//sub menu
void createGameMenu();

//menu for highscores
void createHighscoreMenu();

//Get a list of mapnames
std::vector<std::string> getMapNames(std::string ext);

#endif
