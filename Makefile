CC=g++
CFLAGS=-c -g -std=c++11 -Wall -Wextra -pedantic
LDFLAGS=-lBox2D -lsfml-graphics -lsfml-window -lsfml-system
SOURCES=src/utils.cc src/main.cc src/map.cc src/highscore.cc src/vehicle.cc src/menu.cc src/gamemode.cc src/props.cc
OBJECTS=$(SOURCES:.cc=.o)
EXECUTABLE=hsracing

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

.cc.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)
