#!/bin/bash

# This script will install all required dependencies for both SFML and Box2D,
# then compile them and install them. After that it compiles our game.
#
# It has been tested in a VirtualBox virtual machine, running fresh installs of
# Ubuntu 13.10 as guest OS. Both 32-bit and 64-bit versions of Ubuntu have been
# tested as working.

# SFML deps
sudo apt-get install build-essential cmake libgl1-mesa-dev libxrandr-dev libfreetype6-dev libglew-dev libjpeg-dev libopenal-dev libsndfile1-dev

# Compile/install SFML
wget "http://www.sfml-dev.org/download/sfml/2.1/SFML-2.1-sources.zip"
unzip "SFML-2.1-sources.zip"
cd SFML-2.1
cmake .
make
sudo make install
cd ..

# Box2D deps
sudo apt-get install libxi-dev p7zip-full

# Compile/install Box2D
wget "http://box2d.googlecode.com/files/Box2D_v2.3.0.7z"
7z x "Box2D_v2.3.0.7z"
cd Box2D_v2.3.0/Box2D
cmake .
make
sudo make install
cd ../..

# Create symlinks/cache for new libs
sudo ldconfig -v

# Compile our game
make

